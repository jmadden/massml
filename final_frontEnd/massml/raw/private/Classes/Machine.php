<?php
require("Bundle.php");

class Machine
{
	public  $name;
	public  $ip;
	public  $online;
	public  $id;
	
	private  $pdo;
	public function __construct()
	{
		$this->pdo = $db;
	}

	public function tasksExist()
	{
		$queryOne = $pdo->prepare("SELECT COUNT(*) FROM request WHERE id = :id");
		$queryOne->execute(["id"=>$this->id;]);
		$result = $queryOne->returnColumn();
		if($result > 0)
		{
			return true;		
		} else {
			return false;
		}

	}

	public function assignTask()
	{
		if(tasksExist)
		{
			$request = new Request();	
			$request->popOnParam(["machine_id", $this->id;]);
			$json = json_encode(array('folder_path'=>$request->folder_path));
			set_time_limit(0);
			ob_implicit_flushing();
			$sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP));
			$socket_bind($sock, $this->ip, 8081);
			$msgsock = socket_accept($sock);
			socket_write($msgsock, $json, strlen($json));
		} else {
			return false;
		}
	}
}
