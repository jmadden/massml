<?php
require("Bundle.php");

class Request
{
	public  $id;
	public  $user_id;
	public  $folder_path;
	public  $state;
	public  $machine_id;

	private  $pdo;

	public function __construct()
	{
		$this->pdo = $db;
	}

	public function popOnParam(array $param)
	{
		$queryOne = $pdo->prepare("SELECT * FROM request WHERE " + $param[0] + " = :" + param[0]); 
		$queryOne->execute(param[0] => param[1]);
		$result = $queryOne->fetch(PDO::FETCH_OBJ);
		$this->id = $result->id;
		$this->user_id = $result->user_id;
		$this->folder_path = $result->folder_path;
		$this->state = $result->state;
		$this->machine_id = $result->machine_id;
	}

	public function writeRequest()
	{
		$queryOne = $pdo->prepare("INSERT INTO request (user_id, folder_path, state, machine_id) VALUES(:user_id, :folder_path, :state, :machine_id)");
		$queryOne->execute();
	}
}
