#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <nlohmann/json.hpp>
#include <bits/stdc++.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <cstring>
#include <iostream>
#include <curl/curl.h>
#include <arpa/inet.h>
using json = nlohmann::json;

std::string commandasm(std::string bin, std::string input){
  std::string tmp;
  tmp = bin + input;
  return tmp;
}
int listenport(){
    int new_socket, statusfd, res;
    struct sockaddr_in addr;
    int addrlen = sizeof(addr);
    char buffer[1024]= {0};
    char *reply = "ACK";
    //create a tcp socket
    statusfd = socket(AF_INET, SOCK_STREAM, 0);
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(8081);
    // bind to port 8501
    if (bind(statusfd,(struct sockaddr *)&addr, addrlen)<0)
    {
      printf("failed to bind");
      exit(1);
    }
    for(;;){
    memset(buffer, 0, sizeof(buffer));
    //listen to prot 8501
    listen(statusfd, 0);
    //accept the http post
    new_socket = accept(statusfd, (struct sockaddr *)&addr, (socklen_t*)&addrlen);
    res = read(new_socket,buffer, 1024);
    std::cout << "Command recived" << std::endl;
    json res = json::parse(buffer);
    auto geturl = res["path"];
    std::string command = commandasm("wget", geturl);
    const char *com = command.c_str();
    system(com);
    auto filename = res["file"];
    command = commandasm("python", filename);
    com = command.c_str();
    std::cout << "start training" << std::endl;
    system(com);
  }
}
int inital(std::string ipadd){
  std::cout << "posting" << std::endl;
  std::string postdata = "Status=Ready&Ip="+ipadd;
  CURL *curl;
  CURLcode res;
  curl_global_init(CURL_GLOBAL_ALL);
  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://ptsv2.com/t/yzeyh-1543697718/post");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS,postdata.c_str());
    res = curl_easy_perform(curl);
    if(res != CURLE_OK)
     fprintf(stderr, "curl_easy_perform() failed: %s\n",
             curl_easy_strerror(res));
    curl_easy_cleanup(curl);
    }
  curl_global_cleanup();
}
std::string getip(){
  std::string ip = "192.168.1.100";
  return ip;
}
int main(){
  for(int i=0; i<1; i){
  std::string ip = getip();
  inital(ip);
  listenport();
  }
}
