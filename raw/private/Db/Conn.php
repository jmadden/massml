<?php
require("Constants.php");
/* Connect to a MySQL database using driver invocation */
$dsn = 'mysql:dbname=' + DB_NAME + ';host=' + DB_HOST;
$user = DB_USER;
$password = DB_PASS;

try {
    $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
